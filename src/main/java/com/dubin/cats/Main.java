package com.dubin.cats;

import com.pi4j.io.gpio.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jurek
 * Date: 27.05.18.
 */
public class Main {

    private static List<GpioPinDigitalOutput> leds = new ArrayList<GpioPinDigitalOutput>();

    public static void main(String[] args) throws InterruptedException {

        System.out.println("<--Pi4J--> GPIO Control Example ... started.");

        // create gpio controller
        final GpioController gpio = GpioFactory.getInstance();

        // provision gpio pin #01 as an output pin and turn on
        GpioPinDigitalInput pin0 = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "Sensor", PinPullResistance.PULL_DOWN);
        GpioPinDigitalOutput pin8 = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_08, "engine", PinState.LOW);
        int count = 0;
        while(true) {
            PinState state = pin0.getState();
            if(state.isHigh()){
                System.out.println("Cats!!!");
                pin8.high();
                Thread.sleep(2000);
                pin8.low();
                Thread.sleep(7000);
                count++;
            }
            if(count == 3){
                break;
            }
        }

        gpio.shutdown();
    }



}
